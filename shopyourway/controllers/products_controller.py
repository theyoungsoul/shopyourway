import pyramid_handlers
from shopyourway.controllers.base_controller import BaseController
from shopyourway.services.SearsShopYourWayApi import SearsShopYourWayApi
from shopyourway.viewmodels.product_viewmodel import ProductViewModel


class ProductController(BaseController):
    @pyramid_handlers.action(renderer='templates/products/index.jinja2',
                             request_method='GET',
                             name='index')
    def index_get(self):

        return {
            'products': []
        }

    @pyramid_handlers.action(renderer='templates/products/index.jinja2',
                             request_method='POST',
                             name='index')
    def index_post(self):
        product_type = 'tv'
        limit = 30
        if 'prodtype' in self.request.POST:
            product_type = self.request.POST['prodtype'].strip()

        if 'limit' in self.request.POST:
            limit = self.request.POST['limit']
            if limit != '':
                if limit.isdigit():
                    limit = int(limit)
                else:
                    limit = 5

        products = SearsShopYourWayApi().get_products(product_type=product_type, limit=limit)
        all_products = []
        for prod_json in products:
            prod_vm = ProductViewModel()
            prod_vm.from_dict(prod_json)
            all_products.append(prod_vm)

        return {
            'products': all_products
        }
