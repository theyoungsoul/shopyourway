import pyramid_handlers
from shopyourway.controllers.base_controller import BaseController
from shopyourway.services.SearsShopYourWayApi import SearsShopYourWayApi
from shopyourway.viewmodels.store_viewmodel import StoreViewModel


class StoreController(BaseController):
    zip2LatLon = None

    @pyramid_handlers.action(renderer='templates/stores/index.jinja2',
                             request_method='GET',
                             name='index')
    def index_get(self):

        return {
            'value': 'Store.index.get',
            'stores': [],
            'zipCode': None
        }

    @pyramid_handlers.action(renderer='templates/stores/index.jinja2',
                             request_method='POST',
                             name='index')
    def index_post(self):
        zipcode = None
        if 'zipcode' in self.request.POST:
            zipcode = self.request.POST['zipcode']

        latlon = self.zip2LatLon[zipcode]
        lat = None
        lon = None
        if latlon:
            lat = latlon[0].strip()
            lon = latlon[1].strip()

        stores_json = SearsShopYourWayApi().get_stores_nearby(lat,lon)
        all_stores = []
        for store_json in stores_json:
            store_view_model = StoreViewModel()
            store_view_model.from_dict(store_json)
            all_stores.append(store_view_model)

        return {
            'value': 'Store.index.post',
            'stores': all_stores,
            'zipCode': None
        }
