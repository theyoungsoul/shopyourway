from shopyourway.infrastructure import static_cache
import pyramid.httpexceptions as exc
from shopyourway.infrastructure.supressor import suppress

class BaseController:

    def __init__(self, request):
        self.request = request
        self.build_cache_id = static_cache.build_cache_id
        self.page_title = "Shop Your Way"


    @property
    def is_logged_in(self):
        return False

    @property
    def is_get(self):
        return self.request.method == 'GET'

    @property
    def is_post(self):
        return self.request.method == 'POST'

    @suppress
    def set_title(self, page_title):
        self.page_title = "{} - [{}]".format(page_title, BaseController.page_title)

    def redirect(self, to_url, permanent=False):
        if permanent:
            raise exc.HTTPMovedPermanently(to_url)

        raise exc.HTTPFound(to_url)

