import pyramid_handlers
from shopyourway.controllers.base_controller import BaseController
from shopyourway.viewmodels.register_viewmodel import RegisterViewModel


class AccountController(BaseController):
    @pyramid_handlers.action(renderer='templates/account/index.jinja2')
    def index(self):
        return {}

    @pyramid_handlers.action(renderer='templates/account/signin.jinja2')
    def signin(self):
        return {}

    # GET /account/register
    @pyramid_handlers.action(renderer='templates/account/register.jinja2',
                             request_method='GET',
                             name='register')
    def register_get(self):
        print("Calling register via GET...")
        vm = RegisterViewModel()
        return vm.to_dict()

    # POST /account/register
    @pyramid_handlers.action(renderer='templates/account/register.jinja2',
                             request_method='POST',
                             name='register')
    def register_post(self):
        print("Calling register via POST...")
        vm = RegisterViewModel()
        vm.from_dict(self.request.POST)

        # validate no account exists, passwords match
        vm.validate()
        if vm.error:
            return vm.to_dict()
        # create account in DB
        # send welcome email

        # redirect
        print("Redirecting to account index page...")
        self.redirect('/account')

