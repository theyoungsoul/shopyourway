import pyramid_handlers
from shopyourway.controllers.base_controller import BaseController


class HomeController(BaseController):

    @pyramid_handlers.action(renderer='templates/home/index.jinja2')
    def index(self):
        return {
            'value': 'Home.index'
        }

    @pyramid_handlers.action(renderer='templates/home/about.jinja2')
    def about(self):
        return {
            'value': 'Home.about'
        }

    @pyramid_handlers.action(renderer='templates/home/contact.jinja2')
    def contact(self):
        return {
            'value': 'Home.contact'
        }
