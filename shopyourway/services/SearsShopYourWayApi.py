import requests
import hashlib
from geopy.distance import vincenty
from operator import itemgetter
import pickle
# keys.py
# not part of the repo, you will need to add this file
# app_offline_token = 'your offline token'
# app_offline_hash = 'your offline hash'
from keys import *


class SearsShopYourWayApi:

    def __init__(self, z2ll='./zip2latlon.p'):
        self.zip2latlon = z2ll
        self.base_url = "https://sandboxplatform.shopyourway.com"

    def create_hash(self, token, app_secret):
        new_hash =hashlib.sha256(token+app_secret).hexdigest()
        return new_hash

    def get_canvas_page_url(self):
        canvas_url = requests.get("{}/app/{}/r".format(self.base_url, app_id))
        print(canvas_url)
        return canvas_url

    def get_brand_tag(self, brand_name):
        """

        :param brand_name:  e.g. 'craftsman'
        :return:
        """
        url = "{}/search/tags".format(self.base_url)
        params = {
            'query': brand_name,
            'types': 'brand',
            'token': app_offline_token,
            'hash': app_offline_hash,
        }
        response = requests.get(url, params=params)
        status_code = response.status_code
        json_response = response.json()

        return json_response

    def get_products_by_brand(self, brand_name):
        url = "{}/products/get-by-tag".format(self.base_url)
        params = {
            'tagId': '220431',
            'token': app_offline_token,
            'hash': app_offline_hash,
        }
        response = requests.get(url, params=params)
        status_code = response.status_code
        json_response = response.json()

        return json_response


    def get_stores_by_zipcode(self, zip_code):
        """

        :param brand_name:  e.g. 'craftsman'
        :return:
        """

        zip2LatLon = pickle.load(open(self.zip2latlon, "rb"))
        latlon = zip2LatLon[zip_code]
        lat = None
        lon = None
        if latlon:
            lat = latlon[0].strip()
            lon = latlon[1].strip()

        return self.get_stores_nearby(lat,lon)

    def get_stores_nearby(self, lat, lon):
        url = "{}/stores/nearby".format(self.base_url)
        print(url)
        params = {
            'token': app_offline_token,
            'hash': app_offline_hash,
            'lat': lat, #'42.0616732',
            'lon': lon #'-88.03443879999998'
        }
        response = requests.get(url, params=params)

        status_code = response.status_code

        json_response = response.json()
        if len(json_response) > 0:
            for store in json_response:
                store_lat = store['latitude']
                store_lon = store['longitude']
                store_point = (store_lat, store_lon)
                user_point = (lat, lon)
                distance = vincenty(user_point, store_point).miles
                store['distance'] = distance

        newlist = sorted(json_response, key=itemgetter('distance'), reverse=False) # reverse false means ascending
        return newlist

    def get_stores(self, storeids):

        url = "{}/stores/get?ids=672032&token={}&hash={}".format(self.base_url, app_offline_token,app_offline_hash)
        print(url)
        response = requests.get(url)

        status_code = response.status_code

        json_response = response.json()

        return json_response

    def get_reviews(self, productids):
        response = requests.get("https://sandboxplatform.shopyourway.com/reviews/get?ids=1223")
        status_code = response.status_code
        json_response = response.json()

        return json_response

    def get_products(self, product_type, limit):
        #filter=category:19361
        #brands:Samsung
        #seller:sears.com
        url = "{}/products/search".format(self.base_url)
        params = {
            'token': app_offline_token,
            'hash': app_offline_hash,
            'q': product_type,
            'limit': limit,
            'filter': 'category:19361;brands:Samsung;seller:sears.com'
        }
        response = requests.get(url, params=params)
        status_code = response.status_code
        if status_code != 200:
            print("get products returned status code: {}".format(status_code))
        json_response = response.json()

        if json_response:
            if 'products' not in json_response:
                return []
            else:
                if json_response['products']:
                    return json_response['products']
                else:
                    return []
        else:
            return []
